O sistema criado � um "Dynamic Web Project" simples, usando Eclipse de IDE e hospedado em servidor "TomCat 6.0".
Foi utilizado Servlets para a comunica��o com o servidor.
Foi utilizado Session (Java Web) para o login/autentica��o.
Para a camada de apresenta��o foi utilizado Bootstrap.
O sistema tem apenas uma entrada (index.html).
Todas as demais paginas s�o apenas fragmentos.
Toda a comunica��o entre cliente e servidor � realizada via ajax, de duas formas:
- O conteudo do html atual � susbtituido pelo o que esta sendo solicitado ao servidor.
- Utilizado JSON para a apresenta��o/envio/recebimento de informa��es (uso de lib do google api para a convers�o de classes Java em JSON).
(Como n�o existe a utiliza��o de frameworks de apresenta��o JAVA WEB, o sistema esta livre para ter seu back end substituido a qualquer momento )
Todas os fragmentos de pagina est�o dentro da pasta 'WEB-INF', o que impede que as mesmas sejam acessadas diretamente via http.

PS:N�o foi possivel criar um controle de usuario (o bot�o ficou apenas para dar uma ideia).
