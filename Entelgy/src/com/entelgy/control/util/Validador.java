package com.entelgy.control.util;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class Validador {
        
    private Map<String, String> status = new HashMap<String, String>();

    public Map<String, String> getStatus() {
        return status;
    }

    public boolean isValido() {
        return status.isEmpty();
    }

    public Validador validarStringVazia(String nomeCampo, String valor) {
        if (isEmpty(valor)) {
            status.put(nomeCampo, "O valor deve ser informado.");
        }
        return this;
    }

    public static boolean isEmpty(String valor) {
		return valor == null || valor.trim().isEmpty();
	}

    public Validador validarInteger(String nomeCampo, String valor, boolean isRequirido) {
        try {
        	if (!isEmpty(valor)) {
        		Integer.valueOf(valor);
        	}
        } catch (Exception e) {
             status.put(nomeCampo, "O valor informado deve ser um n�mero inteiro v�lido.");
        }

        if (isRequirido) {
            validarStringVazia(nomeCampo, valor);
        }
        return this;
    }

	public Validador validarDouble(String nomeCampo, String valor, boolean isRequirido) {
		try {
			if (!isEmpty(valor)) {
				new DecimalFormat("#0,00").parse(valor);
			}
        } catch (Exception e) {
             status.put(nomeCampo, "O valor informado deve ser um n�mero v�lido no formato: '9999,99'.");
        }

        if (isRequirido) {
            validarStringVazia(nomeCampo, valor);
        }
        return this;
	}
	
	public Validador validarDecimal(String nomeCampo, String valor, boolean isRequirido) {
		try {
			if (!isEmpty(valor)) {
				Double.parseDouble(valor);
			}
        } catch (Exception e) {
             status.put(nomeCampo, "O valor informado deve ser um n�mero v�lido no formato: '9999.99'.");
        }

        if (isRequirido) {
            validarStringVazia(nomeCampo, valor);
        }
        return this;
	}

	public Validador validarLong(String nomeCampo, String valor, boolean isRequirido) {
		try {
        	if (!isEmpty(valor)) {
        		Long.valueOf(valor);
        	}
        } catch (Exception e) {
             status.put(nomeCampo, "O valor informado deve ser um n�mero inteiro v�lido.");
        }

        if (isRequirido) {
            validarStringVazia(nomeCampo, valor);
        }
        return this;
	}

}
