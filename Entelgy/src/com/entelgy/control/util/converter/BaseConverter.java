package com.entelgy.control.util.converter;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseConverter {
    
    public static final String STATUS = "status";
    
    public static final String OK = "ok";
    
    public static final String FAIL = "fail";
    
    public static final String MESSAGE_ERROR = "error";
    
    public static final String MESSAGE = "msg";
    
    public static Map<String, String> gerarMapaErro(Exception e) {
        Map<String, String> mapa = new HashMap<String, String>();
        mapa.put(STATUS, FAIL);
        mapa.put(MESSAGE_ERROR, e.getMessage());
        return mapa;
    }
    
    public static Map<String, String> gerarMapaMsg(String msg) {
        Map<String, String> mapa = new HashMap<String, String>();
        mapa.put(STATUS, OK);
        mapa.put(MESSAGE, msg);
        return mapa;
    }
    
}
