package com.entelgy.control.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.entelgy.control.util.converter.BaseConverter;
import com.entelgy.model.Usuario;
import com.entelgy.model.Voto;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

@SuppressWarnings("serial")
public abstract class BaseServlet extends HttpServlet {

	private static final String APPLICATION_HTML = "text/html;charset=utf-8";
	private static final String LOCAL_PAGINAS_APP = "/WEB-INF/app/";
	private static final String ERRO_NENHUMA_ACAO = "Nenhuma a��o foi espec�ficada.";
	private static final String ERRO_NENHUMA_ACAO_VALIDA = "Nenhuma a��o v�lida foi espec�ficada.";
	private static final String ERRO_USER_NAO_AUTENTICADO = "Usu�rio n�o esta autenticado, por favor realize o logout da aplica��o e autentique-se novamente.";
	protected static final String APPLICATION_JSON = "application/json; charset=utf-8";
	protected static final String ACAO = "acao";
	protected static final String ATRR_USER = "USER";
	protected Map<String, IAcao> acoes;
	
	protected static final Voto VOTOS = new Voto();
	protected static final List<Usuario> USUARIOS_SISTEMA = new ArrayList<Usuario>();

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doProcessRequest(req, resp);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doProcessRequest(req, resp);
	}

	protected void doProcessRequest(HttpServletRequest req,
			HttpServletResponse resp) throws IOException {
		if (isAcaoInvalida(req)) {
			doMontaMsgErro(resp, ERRO_NENHUMA_ACAO);
		} else if (getAcoes().get(req.getParameter(ACAO)) == null) {
			doMontaMsgErro(resp, ERRO_NENHUMA_ACAO_VALIDA);
		} else {
			try {
				resp.setHeader("Access-Control-Allow-Origin", "*");
		        resp.setHeader("Access-Control-Allow-Methods", "POST");
	            resp.setHeader("Access-Control-Allow-Headers", "Content-Type");
	            resp.setHeader("Access-Control-Max-Age", "86400");
		        resp.setContentType(APPLICATION_JSON);
				doRequest(req, resp);
			} catch (Exception e) {
				doMontaMsgErro(resp, e.getMessage());
			}
		}
	}

	protected void doMontaMsgErro(HttpServletResponse resp, String msg)
			throws IOException {
		resp.setContentType(APPLICATION_JSON);
		resp.getWriter().println(new JSONObject(BaseConverter
								.gerarMapaErro(new Exception(msg))));
	}
	
	protected void doMontaMsg(HttpServletResponse resp, String msg)
			throws IOException {
		doMontaMap(resp, BaseConverter.gerarMapaMsg(msg));
	}
	
	protected void doMontaMap(HttpServletResponse resp, Map<?,?> mapa)
			throws IOException {
		resp.getWriter().println(new JSONObject(mapa));
	}
	
	protected void doRequest(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		getAcoes().get(req.getParameter(ACAO)).executar(req, resp);
	}
	
	protected void doRedirect(String pagina, HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		doBasicRedirect(LOCAL_PAGINAS_APP + pagina, req, resp);
	}
	
	protected void doBasicRedirect(String pagina, HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		resp.setContentType(APPLICATION_HTML);
		req.getRequestDispatcher(pagina).include(req, resp);
	}
	
	protected boolean isUsuarioAutenticado(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		HttpSession session = req.getSession(true);
		Usuario user = (Usuario) session.getAttribute(ATRR_USER);
		if (user == null) {
			doMontaMsgErro(resp, ERRO_USER_NAO_AUTENTICADO);
			return false;
		}
		return true;
	}
	
	protected Map<String, IAcao> getAcoes() {
		if (acoes == null) {
			criarAcoes();
		}
		return acoes;
	}

	private boolean isAcaoInvalida(HttpServletRequest req) {
		return req.getParameter(ACAO) == null
				|| req.getParameter(ACAO).toString().isEmpty();
	}

	protected String formatar(String string, Object... valores) {
		return String.format(string, valores);
	}
	
	protected abstract void criarAcoes();

}
