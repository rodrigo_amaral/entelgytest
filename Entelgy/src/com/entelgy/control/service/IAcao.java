package com.entelgy.control.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IAcao {

	void executar(HttpServletRequest req, HttpServletResponse resp) throws Exception;

}
