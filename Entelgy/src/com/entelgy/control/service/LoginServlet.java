package com.entelgy.control.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.entelgy.control.util.Validador;
import com.entelgy.control.util.converter.BaseConverter;
import com.entelgy.model.Usuario;

@SuppressWarnings("serial")
public class LoginServlet extends BaseServlet {

	@Override
	protected void criarAcoes() {
		acoes = new HashMap<String, IAcao>();
		
		acoes.put("efetuarLogin", new IAcao() {
			@Override
			public void executar(HttpServletRequest req,
					HttpServletResponse resp) throws Exception {
				doAcaoEfetuarLogin(req, resp);
			}
		});
		
		acoes.put("efetuarLogout", new IAcao() {
			@Override
			public void executar(HttpServletRequest req,
					HttpServletResponse resp) throws Exception {
				doAcaoEfetuarLogout(req, resp);
			}
		});
		
		acoes.put("verificarUsuarioLogado", new IAcao() {
			@Override
			public void executar(HttpServletRequest req,
					HttpServletResponse resp) throws Exception {
				doAcaoVerificarUsuarioLogado(req, resp);
			}
		});
		
		acoes.put("cadastrarLogin", new IAcao() {
			@Override
			public void executar(HttpServletRequest req,
					HttpServletResponse resp) throws Exception {
				doAcaoCadastrarLogin(req, resp);
			}
		});
		
		acoes.put("initTelaLogin", new IAcao() {
			@Override
			public void executar(HttpServletRequest req,
					HttpServletResponse resp) throws Exception {
				doRedirect("/login.html", req, resp);
			}
		});
		
		acoes.put("initTelaPrincipal", new IAcao() {
			@Override
			public void executar(HttpServletRequest req,
					HttpServletResponse resp) throws Exception {
				if (isUsuarioAutenticado(req, resp)) {
					doRedirect("principal.html", req, resp);
				}
			}
		});
		
		acoes.put("initTelaIndex", new IAcao() {
			@Override
			public void executar(HttpServletRequest req,
					HttpServletResponse resp) throws Exception {
				doBasicRedirect("/index.html", req, resp);
			}
		});
		
		acoes.put("initTelaCadastroLogin", new IAcao() {
			@Override
			public void executar(HttpServletRequest req,
					HttpServletResponse resp) throws Exception {
				doRedirect("/cadastroLogin.html", req, resp);
			}
		});
		
	}
	
	protected void doAcaoVerificarUsuarioLogado(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		HttpSession session = req.getSession(true);
		Usuario user = (Usuario) session.getAttribute(ATRR_USER);
		if (user != null) {
			Map<String, String> responseJson = BaseConverter
					.gerarMapaMsg("Prezado(a) " + user.getNome() + ", Bem vindo(a) novamente.");
			responseJson.put("jsessionid", session.getId());
			doMontaMap(resp, responseJson);
		} else {
			doMontaMsgErro(resp, "Usu�rio n�o logado.");
		}
		
	}
	
	protected void doAcaoCadastrarLogin(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		
		Validador v = new Validador()
				.validarStringVazia("Nome", req.getParameter("nome"))
				.validarStringVazia("Usu�rio", req.getParameter("usuario"))
				.validarStringVazia("Senha", req.getParameter("senha"));

		if (v.isValido()) {
			Usuario user = verificarNovoUser(req);
			if (user != null) {
				HttpSession session = req.getSession(true);
				session.setAttribute(ATRR_USER, user);
				Map<String, String> responseJson = BaseConverter
						.gerarMapaMsg("Prezado(a) " + user.getNome() + ", Login criado com sucesso.");
				responseJson.put("jsessionid", session.getId());
				doMontaMap(resp, responseJson);
			} else {
				doMontaMsgErro(resp, "Usu�rio / Senha j� existem no sistema.");
			}
		} else {
			doMontaMap(resp, v.getStatus());
		}
	}

	protected void doAcaoEfetuarLogin(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		
		Validador v = new Validador()
				.validarStringVazia("Usu�rio", req.getParameter("usuario"))
				.validarStringVazia("Senha", req.getParameter("senha"));

		if (v.isValido()) {
			Usuario user = verificarUser(req);
			if (user != null) {
				HttpSession session = req.getSession(true);
				session.setAttribute(ATRR_USER, user);
				Map<String, String> responseJson = BaseConverter
						.gerarMapaMsg("Prezado(a) " + user.getNome() + ", Login efetuado com sucesso.");
				responseJson.put("jsessionid", session.getId());
				doMontaMap(resp, responseJson);
			} else {
				doMontaMsgErro(resp, "Usu�rio / Senha incorretos.");
			}
		} else {
			doMontaMap(resp, v.getStatus());
		}
	}
	
	protected void doAcaoEfetuarLogout(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		HttpSession session = req.getSession(true);
		session.removeAttribute(ATRR_USER);
		Map<String, String> responseJson = BaseConverter
				.gerarMapaMsg("Logout efetuado com sucesso.");
		responseJson.put("jsessionid", session.getId());
		doMontaMap(resp, responseJson);
	}

	private Usuario verificarUser(HttpServletRequest req) {
		for (Usuario usuario : USUARIOS_SISTEMA) {
			if (req.getParameter("usuario").equals(usuario.getLogin())
					&& req.getParameter("senha").equals(usuario.getSenha())) {
				return usuario;
			}
		}
		return null;
	}
	
	private Usuario verificarNovoUser(HttpServletRequest req) {
		for (Usuario usuario : USUARIOS_SISTEMA) {
			if (req.getParameter("usuario").equalsIgnoreCase(usuario.getLogin())) {
				return null;
			}
		}
		Usuario userAux = new Usuario();
		userAux.setLogin(req.getParameter("usuario"));
		userAux.setAtivo(true);
		userAux.setNome(req.getParameter("nome"));
		userAux.setSenha(req.getParameter("senha"));
		USUARIOS_SISTEMA.add(userAux);
		return userAux;
	}
}
