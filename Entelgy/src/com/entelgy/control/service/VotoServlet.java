package com.entelgy.control.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.entelgy.control.util.Validador;
import com.entelgy.control.util.converter.BaseConverter;
import com.entelgy.model.ECandidato;
import com.entelgy.model.Usuario;

@SuppressWarnings("serial")
public class VotoServlet extends BaseServlet {

	@Override
	protected void criarAcoes() {
		acoes = new HashMap<String, IAcao>();
		
		acoes.put("votar", new IAcao() {
			@Override
			public void executar(HttpServletRequest req,
					HttpServletResponse resp) throws Exception {
				if (isUsuarioAutenticado(req, resp)) {
					doAcaoVotar(req, resp);
				}
			}
		});
		
		acoes.put("atualizarVotos", new IAcao() {
			@Override
			public void executar(HttpServletRequest req,
					HttpServletResponse resp) throws Exception {
				if (isUsuarioAutenticado(req, resp)) {
					doAcaoAtualizarVotos(req, resp);
				}
			}
		});
		
	}

	protected void doAcaoVotar(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		HttpSession session = req.getSession(true);
		Usuario user = (Usuario) session.getAttribute(ATRR_USER);
		
		Validador v = new Validador()
				.validarInteger("Voto", req.getParameter("voto"), true);

		if (v.isValido()) {
			ECandidato candidato = ECandidato.parse(Integer.parseInt(req.getParameter("voto")));
			if (candidato != null) {
				VOTOS.adicionarVoto(candidato);
				doMontaMsg(resp, "Prezado(a) " + user.getNome() + ", Voto realizado com sucesso.");
			}else {
				doMontaMsgErro(resp, "Candidato n�o existe.");
			}
		} else {
			doMontaMap(resp, v.getStatus());
		}
	}
	
	@SuppressWarnings("unused")
	protected void doAcaoAtualizarVotos(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		HttpSession session = req.getSession(true);
		Usuario user = (Usuario) session.getAttribute(ATRR_USER);
		
		Map<Object, Object> mapRetorno = new HashMap<Object, Object>();
	    for (ECandidato candidato : VOTOS.getVotos().keySet()) {
	    	mapRetorno.put(candidato, VOTOS.getVotos().get(candidato));
		}
	    mapRetorno.put(BaseConverter.STATUS, BaseConverter.OK);
		
		doMontaMap(resp, mapRetorno);
		
	}
}
