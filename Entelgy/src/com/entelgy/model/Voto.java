package com.entelgy.model;

import java.util.HashMap;
import java.util.Map;

public class Voto {
	
	private Map<ECandidato, Integer> votos;

	public Map<ECandidato, Integer> getVotos() {
		if (votos == null) {
			votos = new HashMap<ECandidato, Integer>();
			for (ECandidato candidato : ECandidato.values()) {
				votos.put(candidato, 0);
			}
		}
		return votos;
	}
	
	public void adicionarVoto(ECandidato candidato) {
		getVotos().put(candidato, getVotos().get(candidato) + 1) ;
	}

	public void setVotos(Map<ECandidato, Integer> votos) {
		this.votos = votos;
	}

}
