package com.entelgy.model;

import java.util.HashMap;
import java.util.Map;

public enum ECandidato {
	
	Einstein("Albert Einstein", 1),
	Newton("Isaac Newton", 2),
	Bohr("Niels Bohr", 3),
	Faraday("Michael Faraday", 4),
	Galileu("Galileu Galilei", 5),
	Hawking("Stephen Hawking", 6);

    private int codigo;
    private String descricao;
    private static Map<Integer, ECandidato> elementos;
    
    private ECandidato(String descricao, int codigo) {
        this.setCodigo(codigo);
        this.setDescricao(descricao);
        ECandidato.getElementos().put(codigo, this);
    }
   
    public static Map<Integer, ECandidato> getElementos() {
        if (ECandidato.elementos == null) {
        	ECandidato.elementos = new HashMap<Integer, ECandidato>();
        }
        return ECandidato.elementos;
    }

    public static ECandidato parse(final Integer valor) {
        return ECandidato.getElementos().get(valor);
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

}
