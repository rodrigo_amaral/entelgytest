var App = function() {};
	        
App.get = function() {
    var me = this;
    if (!App._instancia) {
    	App._instancia = new App();
    }
    me = App._instancia;
    return me;
};

App.prototype = {
		
	MSG_AGUARDE : 'Aguarde...'
	, MSG_ERR_JSON : 'Não foi possivel converter o JSON da requisição.'
	, MSG_ERR_TM_OUT : 'Erro de Time Out.'
	, MSG_ERR_N_CON : 'Sem conexão com o servidor, verifique sua rede.'
	, MSG_ERR_404 : 'O recurso solicitado não pode ser encontrado. [404]'
	, MSG_ERR_500 : 'Ops, erro interno do servidor [500].'
	, MSG_ERR_TEC : 'Erro Técnico.'
	, MSG_INF_SEM_REG: 'Nenhum registro a ser exibido.'
	, MSG_INF_SELCT: 'Selecione.'	

	, createMsgLoad : function (strMsg, isCancelar) {
		var img = $('<img/>').attr('src','resources/app/app-imagens/loading.gif').attr('class', 'img-loading');
		var textoMsg = $('<h3/>').attr('class', 'margem-msg-load').html(strMsg ? strMsg : App.get().MSG_AGUARDE);
		var divMsg = null;
		
		if (isCancelar) {
			divMsg = $('<div/>').attr('class', 'alert alert-info alert-dismissible div-load-msg').attr('role', 'alert');
			var btn = $('<button/>').attr('type', 'button').attr('class', 'close').attr('data-dismiss','alert');
			btn.append($('<span/>').attr('aria-hidden','true').html('&times;'));
			btn.append($('<span/>').attr('class','sr-only').html('Close'));
			divMsg.append(btn);
		} else {
			divMsg = $('<div/>').attr('class', 'alert alert-info div-load-msg').attr('role', 'alert');
		}
		divMsg.append(img).append(textoMsg);
    	return divMsg;
	}

	, ajx : function (obj) {
		obj.divMsg = App.get().createMsgLoad(obj.strMsg, obj.isCancelar);
		
		obj.beforeSend = function() {
			$('body').append(obj.divMsg);
			if (obj.fnAntesEnvio) {
				obj.fnAntesEnvio();
			}
		};

		obj.error = function(jqXHR, textStatus, errorThrown) {
			if (textStatus === 'parsererror') {
                alert(App.get().MSG_ERR_JSON);
            } else if (textStatus === 'timeout') {
                alert(App.get().MSG_ERR_TM_OUT);
            } else if (textStatus === 'abort') {
            	return;
            } else if (jqXHR.status === 0) {
                alert(App.get().MSG_ERR_N_CON);
            } else if (jqXHR.status == 404) {
                alert(App.get().MSG_ERR_404);
            } else if (jqXHR.status == 500) {
                alert(App.get().MSG_ERR_500);
            } else {
                alert(App.get().MSG_ERR_TEC + '\n' + jqXHR.responseText);
            }
		};

		obj.complete = function() {
			obj.divMsg.remove();
			if (obj.fnDepoisEnvio) {
				obj.fnDepoisEnvio();
			}
		}

		var ajax = $.ajax(obj);
		obj.divMsg.find('button').click(function() {
			ajax.abort();
			if (obj.fnCancelar) {
				obj.fnCancelar();
			}
		});

		return ajax;
	}
	
	, escreveObjeto : function(data) {
		var str = '';
		for (var k in data) {
			str += k + ': ' + data[k] + '\n';
		}
		return str;
	}
	
	, popularForm : function(frm, data) {   
	    $.each(data, function(key, value) {  
	        var ctrl = $('[name='+key+']', frm);  
	        switch(ctrl.attr("type")) {  
	            case "text" :   
	            case "hidden":  
	            ctrl.val(value);   
	            break;   
	            case "radio" : case "checkbox":   
	            ctrl.each(function(){
	               if($(this).attr('value') == value) {  $(this).attr("checked",value); } });   
	            break;  
	            default:
	            ctrl.val(value); 
	        }  
		});  
	}
	
	, limparForm : function(frm) {
		$(frm).find(':input').each(function() {
		    switch(this.type) {
		        case 'password':
		        case 'text':
		        case 'textarea':
		        case 'file':
		        case 'select-one':
		        case 'hidden':	
		        case 'select-multiple':
		            $(this).val('');
		            break;
		        case 'checkbox':
		        case 'radio':
		        	$(this).attr("checked",false);
		    }
		});
	}
	
	, getFormObj : function(formId) {
	    var formObj = {};
	    var inputs = $('#'+formId).serializeArray();
	    $.each(inputs, function (i, input) {
	        formObj[input.name] = input.value;
	    });
	    return formObj;
	}
	
	, gerarLinhaSemRegistro : function(colspan) {
		return $('<tr/>').append($('<td/>').attr('colspan', colspan).append(App.get().MSG_INF_SEM_REG));
	}
	
	, popularCombo : function(idCombo, strPropVal, strPropLabel, listaJson, defaultValue) {
		var combo = $('#' + idCombo).empty().append(
				$('<option/>').append(App.get().MSG_INF_SELCT).attr('value',''));

		if (!listaJson.length) {
			return;
		}
		for (var i = 0; i < listaJson.length; i++) {
			combo.append($('<option/>')
					.append(listaJson[i][strPropLabel])
					.attr('value', listaJson[i][strPropVal]));
		}
		if (defaultValue) {
			combo.val(defaultValue);
		}
	}
	
};