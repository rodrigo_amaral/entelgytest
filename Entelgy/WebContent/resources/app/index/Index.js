var Index = function() {};
	    
Index.get = function() {
    var me = this;
    if (!Index._instancia) {
    	Index._instancia = new Index();
    }
    me = Index._instancia;
    return me;
};

Index.prototype = {
	
	initTelaPrincipal : function () {
		App.get().ajx({
			strMsg : 'Carregando tela principal...',
			isCancelar : true,
			type : 'POST',
			url : 'login',
			data : {
				acao : 'initTelaPrincipal'
			},
			success : function(data) {
				if (data != null) {
					$('body').empty().append(
							$('<div/>').append($(data)).find('form'));
					$('body').removeAttr('class').addClass('corpo-app-01');
					Voto.get().atualizarVotos();
				}
			}
		});
	}
	
	, initTelaIndex : function () {
		App.get().ajx({
			strMsg : 'Carregando tela de início...',
			isCancelar : true,
			type : 'POST',
			url : 'login',
			data : {
				acao : 'initTelaIndex'
			},
			success : function(data) {
				if (data != null) {
					$('body').empty().append(
							$('<div/>').append($(data)).find('div[id="divIndex"]'));
					$('body').removeAttr('class').addClass('corpo-app-01');
				}
			}
		});
	}
	
	, initTelaLogin : function (el) {
		App.get().ajx({
			strMsg : 'Carregando tela de login...',
			isCancelar : true,
			fnAntesEnvio : function() {
				$(el).attr('disabled', 'disabled');
			},
			fnDepoisEnvio : function() {
				$(el).removeAttr('disabled');
			},
			type : 'POST',
			url : 'login',
			data : {
				acao : 'initTelaLogin'
			},
			success : function(data) {
				if (data != null) {
					$('body').empty().append(
							$('<div/>').append($(data)).find('[id="divContainer"]'));
					$('body').removeAttr('class')
						.addClass('corpo-com-footer').addClass('corpo-app-02');
					$('#txtUsuario').focus();
				}
			}
		});
	}
	
	, initTelaNovoLogin : function (el) {
		App.get().ajx({
			strMsg : 'Carregando tela de cadastro de login...',
			isCancelar : true,
			fnAntesEnvio : function() {
				$(el).attr('disabled', 'disabled');
			},
			fnDepoisEnvio : function() {
				$(el).removeAttr('disabled');
			},
			type : 'POST',
			url : 'login',
			data : {
				acao : 'initTelaCadastroLogin'
			},
			success : function(data) {
				if (data != null) {
					$('body').empty().append(
							$('<div/>').append($(data)).find('[id="divContainer"]'));
					$('body').removeAttr('class')
						.addClass('corpo-com-footer').addClass('corpo-app-02');
					$('#txtNome').focus();
				}
			}
		});
	}
	
};