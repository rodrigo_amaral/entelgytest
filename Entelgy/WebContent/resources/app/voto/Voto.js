var Voto = function() {};
	        
Voto.get = function() {
    var me = this;
    if (!Voto._instancia) {
    	Voto._instancia = new Voto();
    }
    me = Voto._instancia;
    return me;
};

Voto.prototype = {
	
	votar : function (codVoto) {
		var parametros = {};
		parametros['acao'] = 'votar';
		parametros['voto'] = codVoto;
		
		App.get().ajx({
			strMsg : 'Realizando Votação...',
			isCancelar : true,
			type : 'POST',
			url : 'voto',
			dataType : 'json',
			data : parametros,
			success : function(data) {
				if (data != null) {
					if (!data.status) {
						alert(App.get().escreveObjeto(data));
					} else if (data.status == 'ok') {
						alert(data.msg);
						Voto.get().atualizarVotos();
					} else {
						alert(data.error);
					}
				}
			}
		});
	}

	, atualizarVotos : function () {
		App.get().ajx({
			strMsg : 'Atualizando quantidade de votos...',
			isCancelar : true,
			type : 'POST',
			url : 'voto',
			data : {
				acao : 'atualizarVotos'
			},
			success : function(data) {
				if (data != null) {
					if (!data.status) {
						alert(App.get().escreveObjeto(data));
					} else if (data.status == 'ok') {
						for (var k in data) {
							$('#p' + k).html(data[k]);
						}
					} else {
						alert(data.error);
					}
				}
			}
		});
	}
	
	
};