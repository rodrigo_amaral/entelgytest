var Login = function() {};
	        
Login.get = function() {
    var me = this;
    if (!Login._instancia) {
    	Login._instancia = new Login();
    }
    me = Login._instancia;
    return me;
};

Login.prototype = {
	
	efetuarLogin : function () {
		var parametros = App.get().getFormObj('formLogin');
		parametros['acao'] = 'efetuarLogin';
		
		App.get().ajx({
			strMsg : 'Efetuando autenticação...',
			isCancelar : true,
			type : 'POST',
			url : 'login',
			dataType : 'json',
			data : parametros,
			success : function(data) {
				if (data != null) {
					if (!data.status) {
						alert(App.get().escreveObjeto(data));
					} else if (data.status == 'ok') {
						//alert(data.msg);
						Index.get().initTelaPrincipal();
					} else {
						alert(data.error);
					}
				}
			}
		});
	}

	, efetuarLogout : function () {
		App.get().ajx({
			strMsg : 'Efetuando logout...',
			isCancelar : true,
			type : 'POST',
			url : 'login',
			dataType : 'json',
			data : {
				acao : 'efetuarLogout'
			},
			success : function(data) {
				if (data != null) {
					if (!data.status) {
						alert(App.get().escreveObjeto(data));
					} else if (data.status == 'ok') {
						alert(data.msg);
						Index.get().initTelaIndex();
					} else {
						alert(data.error);
					}
				}
			}
		});
	}

	, cadastrarLogin : function () {
		var parametros = App.get().getFormObj('formLogin');
		parametros['acao'] = 'cadastrarLogin';
		
		App.get().ajx({
			strMsg : 'Realizando Cadastro...',
			isCancelar : true,
			type : 'POST',
			url : 'login',
			dataType : 'json',
			data : parametros,
			success : function(data) {
				if (data != null) {
					if (!data.status) {
						alert(App.get().escreveObjeto(data));
					} else if (data.status == 'ok') {
						alert(data.msg);
						Index.get().initTelaPrincipal();
					} else {
						alert(data.error);
					}
				}
			}
		});
	}
	
	, verificarUsuarioLogado : function (el) {
		App.get().ajx({
			strMsg : 'Verificando autenticação...',
			isCancelar : true,
			fnAntesEnvio : function() {
				$(el).attr('disabled', 'disabled');
			},
			fnDepoisEnvio : function() {
				$(el).removeAttr('disabled');
			},
			type : 'POST',
			url : 'login',
			dataType : 'json',
			data : {
				acao : 'verificarUsuarioLogado'
			},
			success : function(data) {
				if (data != null) {
					if (!data.status) {
						alert(App.get().escreveObjeto(data));
					} else if (data.status == 'ok') {
						//alert(data.msg);
						Index.get().initTelaPrincipal();
					} else {
						//alert(data.error);
						Index.get().initTelaLogin();
					}
				}
			}
		});
	}
};